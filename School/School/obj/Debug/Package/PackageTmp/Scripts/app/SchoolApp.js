﻿var schoolApp = angular.module("schoolApp", []);

schoolApp.controller('schoolController', function func($scope, $http) {
    $http({
        method: 'GET',
        url: '/School/GetStudents'
    }).then(function (response) {
        $scope.students = response.data.ListStudents;
        $scope.schoolClasses = response.data.ListSchoolClasses;
    }, function (error) {
        console.log(error);
    });

    $http({
        method: 'GET',
        url: '/School/GetSchoolClasses'
    }).then(function (response) {
        $scope.students = response.data.ListStudents;
        $scope.schoolClasses = response.data.ListSchoolClasses;
    }, function (error) {
        console.log(error);
    });

    $scope.deleteStudent = function (studentId) {
        var cf = confirm("Delete such student");

        if (cf == true)
        {
            $http({
                method: 'POST',
                url: '/School/DeleteStudent',
                data: { studentId }
            }).then(function (response) {
                $scope.students = response.data.ListStudents;
                $scope.schoolClasses = response.data.ListSchoolClasses;
            }, function (error) {
                console.log(error);
            }); 
        }
    }

    $scope.deleteSchoolClass = function (schoolClassId) {
        var cf = confirm("Delete such school class");

        if (cf == true)
        {
            $http({
                method: 'POST',
                url: '/School/DeleteSchooClass',
                data: { schoolClassId }
            }).then(function (response) {
                $scope.students = response.data.ListStudents;
                $scope.schoolClasses = response.data.ListSchoolClasses;
            }, function (error) {
                console.log(error);
            });
        }
    }

    $scope.addStudent = function () {
        var value = prompt("Please enter student name");

        var valueIds = prompt("Please enter ids school class, like 1,2");

        if (value != "" && valueIds != null)
        {
            $http({
                method: 'POST',
                url: '/School/AddStudent',
                data: { nameStudent: value, schoolClassIds: valueIds }
            }).then(function (response) {
                $scope.students = response.data.ListStudents;
                $scope.schoolClasses = response.data.ListSchoolClasses;
            }, function (error) {
                console.log(error);
            });
        }
    }

    $scope.addSchoolClass = function () {
        var value = prompt("Please enter school class name");

        if (value != "") {
            $http({
                method: 'POST',
                url: '/School/AddSchoolClass',
                data: { nameSchoolClass: value }
            }).then(function (response) {
                $scope.schoolClasses = response.data;
            }, function (error) {
                console.log(error);
            });
        }
    }

    $scope.updateSchoolClass = function (schoolClassId) {
        var value = prompt("Please enter new school class name");

        if (value != "") {
            $http({
                method: 'POST',
                url: '/School/UpdateSchoolClass',
                data: { schoolClassId: schoolClassId, nameSchoolClass: value }
            }).then(function (response) {
                $scope.schoolClasses = response.data;
            }, function (error) {
                console.log(error);
            });
        }
    }

    $scope.updateStudent = function (studentId) {
        var value = prompt("Please enter new student name");

        if (value != "") {
            $http({
                method: 'POST',
                url: '/School/UpdateStuden',
                data: { studentId: studentId, nameStudent: value }
            }).then(function (response) {
                $scope.students = response.data;
            }, function (error) {
                console.log(error);
            });
        }
    }
});