﻿using BusinessLogic.DataHelpers;
using Database;
using System.Linq;
using System.Web.Mvc;

namespace School.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}