﻿using BusinessLogic.DataHelpers;
using Database;
using System.Linq;
using System.Web.Mvc;
using PowerMapper;
using School.Models;
using System.Collections.Generic;
using System.Web;
using System;

namespace School.Controllers
{
    public class SchoolController : Controller
    {
        private IBaseDataHelper DataHelper { get; set; }

        public SchoolController(IBaseDataHelper dataHelper)
        {
            DataHelper = dataHelper;
        }

        // GET: School
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetStudents()
        {
            return Json(GetSchoolClassesStudentsHelper(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateStuden(int studentId, string nameStudent)
        {
            var student = DataHelper.All<Student>().FirstOrDefault(x => x.Id == studentId);

            if (student == null)
                throw new HttpException(404, "Student whith provided id not found");

            if (DataHelper.All<Student>().Any(x => x.Name == nameStudent))
                throw new HttpException(404, "Student whith provided name have already exist");

            student.Name = nameStudent;
            DataHelper.Commit();

            return Json(GetStudentsHelper(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddStudent(string nameStudent, string[] schoolClassIds)
        {
            if (DataHelper.All<Student>().Any(x => x.Name == nameStudent && !x.IsDeleted))
                throw new HttpException(404, "Student whith provided name have already exist");

            foreach (var item in schoolClassIds)
            {
                var id = Convert.ToInt32(item);
                if (!DataHelper.All<SchoolClass>().Any(x => x.Id == id))
                    throw new HttpException(404, "School Class whith provided Id not found");
            }

            DataHelper.Add<Student>(new Student { Name = nameStudent });
            DataHelper.Commit();

            var student = DataHelper.All<Student>().FirstOrDefault(x => x.Name == nameStudent && !x.IsDeleted);

            foreach (var item in schoolClassIds)
            {
                var id = Convert.ToInt32(item);
                DataHelper.Add<SchoolStudentRel>(new SchoolStudentRel { StudentId = student.Id, SchoolClassId = id });
            }

            DataHelper.Commit();

            return Json(GetSchoolClassesStudentsHelper(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteStudent(int studentId)
        {
            var student = DataHelper.All<Student>().FirstOrDefault(x => x.Id == studentId && !x.IsDeleted);

            if (student == null)
                throw new HttpException(404, "Student whith provided Id not found");

            student.IsDeleted = true;

            if (student.SchoolStudentRel.Any())
            {
                foreach (var item in student.SchoolStudentRel)
                {
                    DataHelper.Delete<SchoolStudentRel>(item);
                }
            }

            DataHelper.Commit();

            return Json(GetSchoolClassesStudentsHelper(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetSchoolClasses()
        {
            return Json(GetSchoolClassesStudentsHelper(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddSchoolClass(string nameSchoolClass)
        {
            if (DataHelper.All<SchoolClass>().Any(x => x.Name == nameSchoolClass && !x.IsDeleted))
                throw new HttpException(404, "School Class whith provided name have already exist");

            DataHelper.Add<SchoolClass>(new SchoolClass { Name = nameSchoolClass });
            DataHelper.Commit();

            return Json(GetSchoolClassesHelper(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateSchoolClass(int schoolClassId, string nameSchoolClass)
        {
            var schoolClass = DataHelper.All<SchoolClass>().FirstOrDefault(x => x.Id == schoolClassId);

            if (schoolClass == null)
                throw new HttpException(404, "School class whith provided id not found");

            if (DataHelper.All<SchoolClass>().Any(x => x.Name == nameSchoolClass))
                throw new HttpException(404, "School Class whith provided name have already exist");

            schoolClass.Name = nameSchoolClass;
            DataHelper.Commit();

            return Json(GetSchoolClassesHelper(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteSchooClass(int schoolClassId)
        {
            var schoolClass = DataHelper.All<SchoolClass>().FirstOrDefault(x => x.Id == schoolClassId && !x.IsDeleted);

            if (schoolClass == null)
                throw new HttpException(404, "School class whith provided Id not found");

            schoolClass.IsDeleted = true;

            if (schoolClass.SchoolStudentRel.Any())
            {
                foreach (var item in schoolClass.SchoolStudentRel.ToList())
                {
                    item.Student.IsDeleted = true;
                    DataHelper.Delete<SchoolStudentRel>(item);
                }
            }

            DataHelper.Commit();

            return Json(GetSchoolClassesStudentsHelper(), JsonRequestBehavior.AllowGet);
        }

        #region Helpers

        private List<StudentResponseModel> GetStudentsHelper()
        {
            var students = DataHelper.All<Student>().Where(x => !x.IsDeleted);

            List<StudentResponseModel> response = new List<StudentResponseModel>();

            foreach (var student in students)
            {
                response.Add(new StudentResponseModel
                {
                    Id = student.Id,
                    Name = student.Name,
                    SchoolClass = DataHelper.All<SchoolStudentRel>().Count(x => x.StudentId == student.Id),
                });
            }

            return response;
        }

        private List<SchoolClassesResponseModel> GetSchoolClassesHelper()
        {
            var classes = DataHelper.All<SchoolClass>().Where(x => !x.IsDeleted);

            List<SchoolClassesResponseModel> response = new List<SchoolClassesResponseModel>();

            foreach (var schoolclass in classes)
            {
                response.Add(new SchoolClassesResponseModel
                {
                    Id = schoolclass.Id,
                    Name = schoolclass.Name,
                    NumberOfStudents = DataHelper.All<SchoolStudentRel>().Count(x => x.SchoolClassId == schoolclass.Id),
                });
            }

            return response;
        }

        private SchoolClassesStudentsResponseModel GetSchoolClassesStudentsHelper()
        {
            return new SchoolClassesStudentsResponseModel()
            {
                ListSchoolClasses = GetSchoolClassesHelper(),
                ListStudents = GetStudentsHelper(),
            };
        }

        #endregion
    }
}