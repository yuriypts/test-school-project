﻿using Newtonsoft.Json;

namespace School.Models
{
    public class SchoolClassesResponseModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int NumberOfStudents { get; set; }
    }
}