﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace School.Models
{
    public class StudentResponseModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SchoolClass { get; set; }
    }
}