﻿using System.Collections.Generic;

namespace School.Models
{
    public class SchoolClassesStudentsResponseModel
    {
        public List<StudentResponseModel> ListStudents { get; set; }
        public List<SchoolClassesResponseModel> ListSchoolClasses { get; set; }
    }
}