﻿using BusinessLogic.DataHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Unity;

namespace School.App_Start
{
    public static class WebApiConfig
    {
        public static void Register()
        {
            IUnityContainer container = new UnityContainer();

            container.RegisterType<IBaseDataHelper, BaseDataHelper>();

            DependencyResolver.SetResolver(new UnitDependencyResolver(container));
        }

        public class UnitDependencyResolver : IDependencyResolver
        {
            IUnityContainer unitContainer;

            public UnitDependencyResolver(IUnityContainer container)
            {
                unitContainer = container;
            }
            public object GetService(Type serviceType)
            {
                try
                {
                    return unitContainer.Resolve(serviceType);
                }
                catch (Exception)
                {
                    return null;
                }
            }

            public IEnumerable<object> GetServices(Type serviceType)
            {
                try
                {
                    return unitContainer.ResolveAll(serviceType);
                }
                catch (Exception)
                {
                    return new List<object>();
                }   
            }
        }
    }
}