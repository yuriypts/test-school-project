﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BusinessLogic.DataHelpers
{
    public interface IBaseDataHelper
    {
        void Commit();
        IQueryable<T> All<T>() where T : class;
        void Add<T>(T obj) where T : class;
        void Delete<T>(T item) where T : class;
    }
}
