﻿using Database;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BusinessLogic.DataHelpers
{
    public class BaseDataHelper : IBaseDataHelper
    {
        protected SchoolEntities Entities { get; set; }

        public BaseDataHelper(SchoolEntities entities)
        {
            Entities = entities;
        }

        public IQueryable<T> All<T>()  where T : class
        {
            return Entities.Set<T>();
        }

        public void Commit()
        {
            try
            {
                Entities.SaveChanges();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool Exists<T>(T entity) where T : class
        {
            return Entities.Set<T>().Any(x => x == entity);
        }

        public void Add<T>(T obj) where T : class
        {
            Entities.Set<T>().Add(obj);
        }

        public void Delete<T>(T item) where T : class
        {
            Entities.Set<T>().Remove(item);
        }
    }
}
